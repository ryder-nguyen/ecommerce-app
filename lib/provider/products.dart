import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce_app/models/product.dart';
import 'package:flutter/cupertino.dart';

class Products with ChangeNotifier {
  List<Product> _products = [];
  List<Product> get products => _products;

  List<String> _banners = [];
  List<String> get banners => _banners;

  List<String> _brands = [];
  List<String> get brands => _brands;

  Future<void> fetchProducts() async {
    await FirebaseFirestore.instance.collection('products').get().then(
            (QuerySnapshot productsSnapshot) {

              _products =[];
              productsSnapshot.docs.forEach((element) async {
                _products.insert(0, Product(
                    id: element.get('productId'),
                    title: element.get('productTitle'),
                    description: element.get('productDescription'),
                    price: double.parse(element.get('price')),
                    imageUrl: element.get('productImage'),
                    brand: element.get('productBrand'),
                    productCategoryName: element.get('productCategory'),
                    quantity: int.parse(element.get('productQuantity')),
                    isPopular: true));
              });
            }
    );

    print('Fetch products');

  }

  Future<void> fetchBanners() async {
    await FirebaseFirestore.instance.collection('banners').get().then(
            (QuerySnapshot bannerSnapshot) {

          _banners =[];
          bannerSnapshot.docs.forEach((element) async{
            _banners.insert(0, element.get('imageUrl'));
          });
        }
    );

    print('Fetch Banners');
  }

  Future<void> fetchBranches() async {
    await FirebaseFirestore.instance.collection('branchs').get().then(
            (QuerySnapshot bannerSnapshot) {
          _brands =[];
          bannerSnapshot.docs.forEach((element) async {
            _brands.insert(0, element.get('name'));
          });
        }
    );

    print('Fetch Brands');
  }

  List<Product> findByCategory(String categoryName) {
    List _categoryList = _products
        .where((element) => element.productCategoryName
            .toLowerCase()
            .contains(categoryName.toLowerCase()))
        .toList();
    return _categoryList;
  }

  List<Product> findByBrand(String brand) {
    List _categoryList = _products
        .where((element) => element.brand
        .toLowerCase()
        .contains(brand.toLowerCase()))
        .toList();
    return _categoryList;
  }

  Product findById(String productId) {
    return _products.firstWhere((element) => element.id == productId);
  }

  List<Product> get popularProducts {
    return _products.where((element) => element.isPopular).toList();
  }

  List<Product> searchQuery(String searchText) {
    List _searchList = _products
        .where((element) =>
        element.title.toLowerCase().contains(searchText.toLowerCase()))
        .toList();
    return _searchList;
  }
}
