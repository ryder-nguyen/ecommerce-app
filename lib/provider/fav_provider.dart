import 'package:ecommerce_app/models/fav_attr.dart';
import 'package:ecommerce_app/models/product.dart';
import 'package:flutter/cupertino.dart';

class FavProvider with ChangeNotifier {
  Map<String, FavAttr> _favItems = {};

  Map<String, FavAttr> get getFavItems {
    return {..._favItems};
  }

  void addFav(Product product) {
    if (!_favItems.containsKey(product.id)) {
      _favItems.putIfAbsent(
          product.id,
          () => FavAttr(
              id: DateTime.now().toString(),
              title: product.title,
              price: product.price,
              imageUrl: product.imageUrl,
              productId: product.id));
    }
    notifyListeners();
  }

  void removeItemFav(String productId) {
    if (_favItems.containsKey(productId)) {
      _favItems.remove(productId);
    }
    notifyListeners();
  }
}
