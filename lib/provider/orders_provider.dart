import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce_app/models/order_arr.dart';
import 'package:ecommerce_app/models/product.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class OrdersProvider with ChangeNotifier {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  List<OrderAttr> _orders = [];
  List<OrderAttr> get orders {
    return [..._orders];
  }

  Future<void> fetchOrders() async {
    await FirebaseFirestore.instance
        .collection('orders')
        .where('userId', isEqualTo: _auth.currentUser.uid)
        .get()
        .then((QuerySnapshot ordersSnapshot) {
      _orders.clear();
      ordersSnapshot.docs.forEach((element) {
        _orders.insert(
            0,
            OrderAttr(
              orderId: element.get('orderId'),
              productId: element.get('productId'),
              userId: element.get('userId'),
              price: element.get('price').toString(),
              quantity: element.get('quantity').toString(),
              imageUrl: element.get('imageUrl'),
              title: element.get('title'),
              orderDate: element.get('orderDate'),
            ));
      });
    });
  }
}
