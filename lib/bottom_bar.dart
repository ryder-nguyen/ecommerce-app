import 'package:ecommerce_app/screens/cart/cart.dart';
import 'package:ecommerce_app/screens/feeds.dart';
import 'package:ecommerce_app/screens/home.dart';
import 'package:ecommerce_app/screens/search.dart';
import 'package:ecommerce_app/screens/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'constants/my_icons.dart';

class BottomBarScreen extends StatefulWidget {
  static const routeName = "/BottomBarScreen";


  @override
  _BottomBarScreenState createState() => _BottomBarScreenState();
}

class _BottomBarScreenState extends State<BottomBarScreen> {
  List<Widget> _pages = [HomeScreen(), FeedScreen(), Search(), CartScreen(), UserScreen()];
  int _selectedIndex = 0;

  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _selectedIndex);
  }

  void _selectedPage(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.jumpToPage(_selectedIndex);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        children: _pages,
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
      ),
      bottomNavigationBar: BottomAppBar(
        clipBehavior: Clip.antiAlias,
        notchMargin: 0.01,
        shape: CircularNotchedRectangle(),
        child: Container(
          decoration: BoxDecoration(
              border: Border(top: BorderSide(width: 0.5, color: Colors.grey)),
              color: Colors.white),
          child: BottomNavigationBar(
            selectedLabelStyle: GoogleFonts.mcLaren(),
            unselectedLabelStyle: GoogleFonts.mcLaren(),
            onTap: _selectedPage,
            backgroundColor: Theme.of(context).primaryColor,
            unselectedItemColor: Theme.of(context).textSelectionColor,
            selectedItemColor: Colors.purple,
            currentIndex: _selectedIndex,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(MyAppIcons.home), label: 'Home'),
              BottomNavigationBarItem(
                  icon: Icon(MyAppIcons.rss), label: 'Hàng hot'),
              BottomNavigationBarItem(
                  icon: Icon(null), label: 'Tìm kiếm', activeIcon: null),
              BottomNavigationBarItem(
                  icon: Icon(MyAppIcons.cart), label: 'Giỏ'),
              BottomNavigationBarItem(
                  icon: Icon(MyAppIcons.user), label: 'Cá nhân'),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation:
      FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FloatingActionButton(
          hoverElevation: 10,
          backgroundColor: Colors.purple,
          splashColor: Colors.grey,
          tooltip: 'Tìm kiếm',
          elevation: 4,
          child: Icon(MyAppIcons.search),
          onPressed: () {
            _selectedPage(2);
          },
        ),
      ),
    );
  }
}
