import 'package:flutter/cupertino.dart';

class UserModel {
  final String id;
  final String name;
  final String email;
  final int phoneNumber;
  final String address;
  final String joinedAt;
  final String imageUrl;

  UserModel({this.id, this.name, this.email, this.phoneNumber, this.address,
      this.joinedAt, this.imageUrl});
}
