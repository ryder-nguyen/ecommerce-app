import 'package:ecommerce_app/inner_screens/product_details.dart';
import 'package:ecommerce_app/models/cart_arr.dart';
import 'package:ecommerce_app/models/product.dart';
import 'package:ecommerce_app/provider/cart_provider.dart';
import 'package:ecommerce_app/provider/dark_theme_provider.dart';
import 'package:ecommerce_app/provider/fav_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class PopularProducts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeChange = Provider.of<DarkThemeProvider>(context);
    final productsAttributes = Provider.of<Product>(context);
    final cartProvider = Provider.of<CartProvider>(context);
    final favProvider = Provider.of<FavProvider>(context);

    return Padding(
      padding: EdgeInsets.all(8),
      child: Container(
        width: 250,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10))),
        child: Material(
          color: Colors.transparent,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          child: InkWell(
            onTap: () => Navigator.of(context).pushNamed(
                ProductDetails.routeName,
                arguments: '${productsAttributes.id}'),
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 170,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(
                              productsAttributes.imageUrl,
                            ),
                            fit: BoxFit.cover),
                      ),
                    ),
                    Positioned(
                      right: 10,
                      top: 7,
                      child: Icon(
                        Entypo.star_outlined,
                        color: favProvider.getFavItems.containsKey(productsAttributes.id) ? Colors.red : Colors.white,
                      ),
                    ),
                    Positioned(
                      bottom: 12,
                      right: 12,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        color: Theme.of(context).backgroundColor,
                        child: Text(
                          '${productsAttributes.price} ',
                          style: GoogleFonts.mcLaren(
                              color: Theme.of(context).textSelectionColor),
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        productsAttributes.title,
                        maxLines: 1,
                        style: GoogleFonts.mcLaren(
                            fontSize: 18, fontWeight: FontWeight.bold,
                            color: themeChange.darkMode ? Colors.white : Colors.black
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              '${productsAttributes.description}',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.mcLaren(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: themeChange.darkMode ? Colors.white : Colors.grey[800]),
                            ),
                            flex: 5,
                          ),
                          Spacer(),
                          Expanded(
                            flex: 1,
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: cartProvider.getCartItems.containsKey(productsAttributes.id)
                                    ? () {}
                                    : () {
                                        cartProvider.addProductToCart(
                                            productsAttributes.id,
                                            productsAttributes.price,
                                            productsAttributes.title,
                                            productsAttributes.imageUrl);
                                      },
                                borderRadius: BorderRadius.circular(30.0),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    cartProvider.getCartItems.containsKey(productsAttributes.id) ?
                                    MaterialCommunityIcons.check_all :
                                    MaterialCommunityIcons.cart_plus,
                                    size: 25,
                                    color: themeChange.darkMode ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
