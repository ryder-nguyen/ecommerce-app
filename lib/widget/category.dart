import 'package:ecommerce_app/screens/categories_feed.dart';
import 'package:ecommerce_app/screens/feeds.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CategoryWidget extends StatelessWidget {
  final int index;

  List<Map<String, Object>> categories = [
    {
      'categoryName': 'Đồng hồ',
      'categoryImagePath': 'assets/images/CatWatches.jpg'
    },
    {
      'categoryName': 'Laptop',
      'categoryImagePath': 'assets/images/CatLaptops.png'
    },
    {
      'categoryName': 'Điện thoại',
      'categoryImagePath': 'assets/images/CatPhones.jpg'
    },
    {
      'categoryName': 'Phụ kiện',
      'categoryImagePath': 'assets/images/accessories.png'
    },
  ];

  CategoryWidget({Key key, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(CategoriesFeedScreen.routeName,
              arguments: '${categories[index]['categoryName']}');
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage(categories[index]['categoryImagePath']),
                  fit: BoxFit.cover)),
          margin: EdgeInsets.symmetric(horizontal: 10),
          width: 150,
          height: 150,
        ),
      ),
      Positioned(
          bottom: 0,
          left: 0,
          right: 10,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            color: Theme.of(context).backgroundColor,
            child: Text(
              categories[index]['categoryName'],
              style: GoogleFonts.mcLaren(
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                  color: Theme.of(context).textSelectionColor),
            ),
          ))
    ]);
  }
}
