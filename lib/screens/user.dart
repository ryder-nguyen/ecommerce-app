import 'dart:io';

import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/constants/my_icons.dart';
import 'package:ecommerce_app/models/user_model.dart';
import 'package:ecommerce_app/provider/dark_theme_provider.dart';
import 'package:ecommerce_app/screens/orders/order.dart';
import 'package:ecommerce_app/screens/wishlist/wishlist.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:list_tile_switch/list_tile_switch.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import 'cart/cart.dart';

class UserScreen extends StatefulWidget {
  static const routeName = '/UserScreen';
  @override
  _UserInfoState createState() => _UserInfoState();
}

class _UserInfoState extends State<UserScreen> with AutomaticKeepAliveClientMixin<UserScreen> {
  ScrollController _scrollController;
  var top = 0.0;
  final _globalMethod = GlobalMethod();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  UserModel _userModel = GlobalMethod.userModel;
  File _pickedImage;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      setState(() {});
    });
    print('name ${_userModel.name}');
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final themeChange = Provider.of<DarkThemeProvider>(context);
    return Scaffold(
      body: Stack(
        children: [
          CustomScrollView(
            controller: _scrollController,
            slivers: <Widget>[
              SliverAppBar(
                automaticallyImplyLeading: false,
                elevation: 4,
                expandedHeight: 200,
                pinned: true,
                flexibleSpace: LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints constraints) {
                  top = constraints.biggest.height;
                  return Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            MyColors.starterColor,
                            MyColors.endColor,
                          ],
                          begin: const FractionalOffset(0.0, 0.0),
                          end: const FractionalOffset(1.0, 0.0),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp),
                    ),
                    child: FlexibleSpaceBar(
                      collapseMode: CollapseMode.parallax,
                      centerTitle: true,
                      title: Row(
                        //  mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          AnimatedOpacity(
                            duration: Duration(milliseconds: 300),
                            opacity: top <= 110.0 ? 1.0 : 0,
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 12,
                                ),
                                Container(
                                  height: kToolbarHeight / 1.8,
                                  width: kToolbarHeight / 1.8,
                                  decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.white,
                                        blurRadius: 1.0,
                                      ),
                                    ],
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: _pickedImage == null ?
                                      NetworkImage(_userModel.imageUrl) :  FileImage(_pickedImage),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 12,
                                ),
                                Text(
                                  // 'top.toString()',
                                  _userModel.name == null ? 'Khách' : _userModel.name,
                                  style: GoogleFonts.mcLaren(
                                      fontSize: 20.0, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      background: Image(
                        image: _pickedImage == null ?
                        NetworkImage(_userModel.imageUrl) :  FileImage(_pickedImage),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                }),
              ),
              SliverToBoxAdapter(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: userTitle('Cá nhân')),
                    Divider(
                      thickness: 1,
                      color: Colors.grey,
                    ),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Theme.of(context).splashColor,
                        child: ListTile(
                          onTap: () => Navigator.of(context)
                              .pushNamed(WishListScreen.routeName),
                          title: Text('Yêu thích', style: GoogleFonts.mcLaren(),),
                          trailing: Icon(Icons.chevron_right_rounded),
                          leading: Icon(MyAppIcons.wishlist),
                        ),
                      ),
                    ),

                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Theme.of(context).splashColor,
                        child: ListTile(
                          onTap: () => Navigator.of(context)
                              .pushNamed(OrderScreen.routeName),
                          title: Text('Lịch sử mua hàng', style: GoogleFonts.mcLaren(),),
                          trailing: Icon(Icons.chevron_right_rounded),
                          leading: Icon(Icons.card_travel),
                        ),
                      ),
                    ),

                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Theme.of(context).splashColor,
                        child: ListTile(
                          onTap: () => Navigator.of(context)
                              .pushNamed(CartScreen.routeName),
                          title: Text('Giở hàng', style: GoogleFonts.mcLaren()),
                          trailing: Icon(Icons.chevron_right_rounded),
                          leading: Icon(MyAppIcons.cart),
                        ),
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: userTitle('Thông tin cá nhân')),
                    Divider(
                      thickness: 1,
                      color: Colors.grey,
                    ),
                    userListTile('Email', _userModel.email ?? '', 0, context),
                    userListTile(
                        'Điện thoại', '${_userModel.phoneNumber ?? ''}', 1, context),
                    userListTile('Địa chỉ nhận hàng', '${_userModel.address}', 2, context),
                    userListTile('Ngày tham gia', _userModel.joinedAt ?? '', 3, context),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: userTitle('Cài đặt'),
                    ),
                    Divider(
                      thickness: 1,
                      color: Colors.grey,
                    ),
                    ListTileSwitch(
                      value: themeChange.darkMode,
                      leading: Icon(Ionicons.md_moon),
                      onChanged: (value) {
                        setState(() {
                          themeChange.darkMode = value;
                        });
                      },
                      visualDensity: VisualDensity.comfortable,
                      switchType: SwitchType.cupertino,
                      switchActiveColor: Colors.indigo,
                      title: Text('Chế độ tối',  style: GoogleFonts.mcLaren()),
                    ),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Theme.of(context).splashColor,
                        child: ListTile(
                          onTap: () async {
                            _globalMethod.confirmSignOut(context, () async {
                              await _auth.signOut();
                            });
                          },
                          title: Text('Đăng xuất',  style: GoogleFonts.mcLaren()),
                          leading: Icon(Icons.exit_to_app_rounded),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          _buildFab()
        ],
      ),
    );
  }

  Widget _buildFab() {
    //starting fab position
    final double defaultTopMargin = 200.0 - 4.0;
    //pixels from top where scaling should start
    final double scaleStart = 160.0;
    //pixels from top where scaling should end
    final double scaleEnd = scaleStart / 2;

    double top = defaultTopMargin;
    double scale = 1.0;
    if (_scrollController.hasClients) {
      double offset = _scrollController.offset;
      top -= offset;
      if (offset < defaultTopMargin - scaleStart) {
        //offset small => don't scale down
        scale = 1.0;
      } else if (offset < defaultTopMargin - scaleEnd) {
        //offset between scaleStart and scaleEnd => scale down
        scale = (defaultTopMargin - scaleEnd - offset) / scaleEnd;
      } else {
        //offset passed scaleEnd => hide fab
        scale = 0.0;
      }
    }

    return Positioned(
      top: top + 25,
      right: 16.0,
      child: Transform(
        transform: Matrix4.identity()..scale(scale),
        alignment: Alignment.center,
        child: FloatingActionButton(
          backgroundColor: Colors.purple,
          heroTag: "btn1",
          onPressed: () async {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text(
                      'Lựa chọn',
                      style: GoogleFonts.mcLaren(
                          fontWeight: FontWeight.w600,
                          color: MyColors.gradiendLStart),
                    ),
                    content: SingleChildScrollView(
                      child: ListBody(
                        children: [
                          InkWell(
                            onTap: _pickImageCamera,
                            splashColor: Colors.purpleAccent,
                            child: Row(
                              children: [
                                Padding(
                                    padding:
                                    EdgeInsets.all(8),
                                    child: Icon(
                                      Icons.camera,
                                      color:
                                      Colors.purpleAccent,
                                    )),
                                Text(
                                  'Camera',
                                  style: GoogleFonts.mcLaren(
                                      fontSize: 18,
                                      color: MyColors.title,
                                      fontWeight:
                                      FontWeight.w500),
                                )
                              ],
                            ),
                          ),
                          InkWell(
                            onTap: _pickImageGallery,
                            splashColor: Colors.purpleAccent,
                            child: Row(
                              children: [
                                Padding(
                                    padding:
                                    EdgeInsets.all(8),
                                    child: Icon(
                                      Icons.image,
                                      color:
                                      Colors.purpleAccent,
                                    )),
                                Text(
                                  'Thư viện ảnh',
                                  style: GoogleFonts.mcLaren(
                                      fontSize: 18,
                                      color: MyColors.title,
                                      fontWeight:
                                      FontWeight.w500),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                });
          },
          child: Icon(Icons.camera_alt_outlined),
        ),
      ),
    );
  }

  List<IconData> _userTileIcons = [
    Icons.email,
    Icons.phone,
    Icons.local_shipping,
    Icons.watch_later,
    Icons.exit_to_app_rounded
  ];

  Widget userListTile(
      String title, String subTitle, int index, BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Theme.of(context).splashColor,
        child: ListTile(
          onTap: () {},
          title: Text(title, style: GoogleFonts.mcLaren()),
          subtitle: Text(subTitle, style: GoogleFonts.mcLaren()),
          leading: Icon(_userTileIcons[index]),
        ),
      ),
    );
  }

  Widget userTitle(String title) {
    return Padding(
      padding: const EdgeInsets.all(14.0),
      child: Text(
        title,
        style: GoogleFonts.mcLaren(fontWeight: FontWeight.bold, fontSize: 23),
      ),
    );
  }

  void updateAvatar() async{
    final ref = FirebaseStorage.instance.ref().child('usersimages')
        .child(Uuid().v1() + '.jpg');
    await ref.putFile(_pickedImage);
    var url = await ref.getDownloadURL();
    final User user = _auth.currentUser;
    await user.updateProfile(photoURL: url);
    print('imageURL: $url');
    user.reload();
  }

  void _pickImageCamera() async {
    final picker = ImagePicker();
    final pickedImage = await picker.getImage(source: ImageSource.camera, imageQuality: 10);
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });
    Navigator.pop(context);
    updateAvatar();
  }

  void _pickImageGallery() async {
    final picker = ImagePicker();
    final pickedImage = await picker.getImage(source: ImageSource.gallery, imageQuality: 10);
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });
    Navigator.pop(context);
    updateAvatar();
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
