import 'package:ecommerce_app/screens/landing_page.dart';
import 'package:ecommerce_app/screens/main_screen.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class UserState extends StatelessWidget {
  GlobalMethod _globalMethod = GlobalMethod();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseAuth.instance.authStateChanges(),
      // ignore: missing_return
      builder: (context, userSnapshot){
        if(userSnapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator(),);
        } else if(userSnapshot.connectionState == ConnectionState.active) {
          if(userSnapshot.hasData) {
            if(GlobalMethod.userModel == null) {
              _globalMethod.saveData();
            }
            return MainScreens();
          } else {
            _globalMethod.resetData();
            return LandingPage();
          }
        } else if(userSnapshot.hasError) {
          return Center(
            child: Text('Có lỗi xảy ra!'),
          );
        }
      } ,
    );
  }
}
