import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce_app/bottom_bar.dart';
import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/screens/auth/login.dart';
import 'package:ecommerce_app/screens/auth/sign_up.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;
  List<String> images = [
    'https://firebasestorage.googleapis.com/v0/b/shopapp-fc69e.appspot.com/o/backgroundimages%2FThe-Best-Things-to-Buy-in-August-2021.jpg?alt=media&token=ead1e77c-0e39-40e8-beb9-e945f7389bb4',
    'https://firebasestorage.googleapis.com/v0/b/shopapp-fc69e.appspot.com/o/backgroundimages%2Fistockphoto-1148923865-612x612.jpg?alt=media&token=f3483277-1a18-438c-9502-5f7746355342',
    'https://firebasestorage.googleapis.com/v0/b/shopapp-fc69e.appspot.com/o/backgroundimages%2FSouthPark-happyreturns-fb_d4_20210219132514.jpg?alt=media&token=f032f94a-d0db-4619-b63a-aa49aac48efc'
  ];

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final _globalMethod = GlobalMethod();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 20));
    _animation =
        CurvedAnimation(parent: _animationController, curve: Curves.linear)
          ..addListener(() {
            setState(() {});
          })
          ..addStatusListener((animationStatus) {
            if (animationStatus == AnimationStatus.completed) {
              _animationController.reset();
              _animationController.forward();
            }
          });
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Future<void> _googleSignIn() async {
    final googleSignIn = GoogleSignIn();
    final googleAccount = await googleSignIn.signIn();
    if (googleAccount != null) {
      try {
        final googleAuth = await googleAccount.authentication;
        if (googleAuth.accessToken != null && googleAuth.idToken != null) {
          final authResult = await _auth.signInWithCredential(
              GoogleAuthProvider.credential(
                  idToken: googleAuth.idToken,
                  accessToken: googleAuth.accessToken));

          var date = DateTime.now().toString();
          var dateParse = DateTime.parse(date);
          var formattedDate =
              "${dateParse.year}-${dateParse.month}-${dateParse.year}";
          await FirebaseFirestore.instance
              .collection('users')
              .doc(authResult.user.uid)
              .set({
            'id': authResult.user.uid,
            'name': authResult.user.displayName,
            'email': authResult.user.email,
            'phoneNumber': authResult.user.phoneNumber,
            'imageUrl': authResult.user.photoURL,
            'joinedAt': formattedDate,
            'created': Timestamp.now()
          });
        }
      } catch (error) {
        _globalMethod.showAuthError(error.message, context);
      }
    }
  }

  void _loginAnonymous() async {
    setState(() {
      _isLoading = true;
    });

    try {
      await _auth.signInAnonymously();
    } catch (error) {
      _globalMethod.showAuthError('${error.message}', context);
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            CachedNetworkImage(
              imageUrl: images[0],
              placeholder: (context, url) => Image.network(''),
              fit: BoxFit.cover,
              errorWidget: (context, url, error) => Icon(Icons.error),
              height: double.infinity,
              width: double.infinity,
              alignment: FractionalOffset(_animation.value, 0),
            ),
            Center(
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 30),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      '21th Store'.toUpperCase(),
                      style: GoogleFonts.mcLaren(
                        fontSize: 40,
                        color: Colors.white
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      child: Text(
                        'Cái gì cũng có!'.toUpperCase(),
                        style: GoogleFonts.mcLaren(
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: ElevatedButton(
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30),
                                      side: BorderSide(
                                          color: MyColors.backgroundColor)))),
                          onPressed: () {
                            Navigator.pushNamed(context, LoginScreen.routeName);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Đăng nhập',
                                style: GoogleFonts.mcLaren(
                                    fontWeight: FontWeight.w500, fontSize: 17),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                Feather.user_plus,
                                size: 18,
                              )
                            ],
                          )),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  Colors.pink.shade400),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30),
                                      side: BorderSide(
                                          color: MyColors.backgroundColor)))),
                          onPressed: () {
                            Navigator.pushNamed(
                                context, SignUpScreen.routeName);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Đăng ký',
                                style: GoogleFonts.mcLaren(
                                    fontWeight: FontWeight.w500, fontSize: 17),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                Feather.user,
                                size: 18,
                              )
                            ],
                          )),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Divider(
                          color: Colors.white,
                          thickness: 2,
                        ),
                      ),
                    ),
                    Text(
                      'Hoặc tiếp tục với',
                      style: GoogleFonts.mcLaren(color: Colors.white),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Divider(
                          color: Colors.white,
                          thickness: 2,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    OutlineButton(
                      onPressed: _googleSignIn,
                      shape: StadiumBorder(),
                      highlightedBorderColor: Colors.red.shade200,
                      borderSide: BorderSide(width: 2, color: Colors.red),
                      child: Text(
                        'Google +',
                        style: GoogleFonts.mcLaren(color: Colors.red),
                      ),
                    ),
                    _isLoading
                        ? CircularProgressIndicator()
                        : OutlineButton(
                            onPressed: _loginAnonymous,
                            shape: StadiumBorder(),
                            highlightedBorderColor: Colors.deepPurple.shade200,
                            borderSide:
                                BorderSide(width: 2, color: Colors.deepPurple),
                            child: Text('Đăng nhập khách',
                                style: GoogleFonts.mcLaren(color: Colors.deepPurple)),
                          ),
                  ],
                ),
                SizedBox(
                  height: 40,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
