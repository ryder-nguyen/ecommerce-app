import 'package:badges/badges.dart';
import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/constants/my_icons.dart';
import 'package:ecommerce_app/models/product.dart';
import 'package:ecommerce_app/provider/cart_provider.dart';
import 'package:ecommerce_app/provider/fav_provider.dart';
import 'package:ecommerce_app/provider/products.dart';
import 'package:ecommerce_app/screens/wishlist/wishlist.dart';
import 'package:ecommerce_app/widget/feed_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'cart/cart.dart';

class FeedScreen extends StatefulWidget {
  static const routeName = '/feeds';

  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen>
    with AutomaticKeepAliveClientMixin<FeedScreen> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    final productsProvider = Provider.of<Products>(context);
    List<Product> productsList;

    if (productsList == 'popular') {
      productsList = productsProvider.popularProducts;
    } else {
      productsList = productsProvider.products;
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).cardColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Hàng Hot'.toUpperCase(),
          style:
              GoogleFonts.mcLaren(fontSize: 16, fontWeight: FontWeight.normal),
        ),
        actions: [
          Consumer<FavProvider>(
            builder: (_, favsProvider, ch) => Badge(
              badgeColor: MyColors.cartBadgeColor,
              animationType: BadgeAnimationType.slide,
              toAnimate: true,
              position: BadgePosition.topEnd(top: 5, end: 7),
              badgeContent: Text(
                favsProvider.getFavItems.length.toString(),
                style: GoogleFonts.mcLaren(color: Colors.white),
              ),
              child: IconButton(
                icon: Icon(
                  MyAppIcons.wishlist,
                  color: MyColors.favColor,
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed(
                    WishListScreen.routeName,
                  );
                },
              ),
            ),
          ),
          Consumer<CartProvider>(
              builder: (_, cartsProvider, ch) => Badge(
                    badgeColor: MyColors.cartBadgeColor,
                    animationType: BadgeAnimationType.slide,
                    toAnimate: true,
                    position: BadgePosition.topEnd(top: 5, end: 7),
                    badgeContent: Text(
                      cartsProvider.getCartItems.length.toString(),
                      style: GoogleFonts.mcLaren(color: Colors.white),
                    ),
                    child: IconButton(
                      icon: Icon(
                        MyAppIcons.cart,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed(CartScreen.routeName);
                      },
                    ),
                  ))
        ],
      ),
      body: RefreshIndicator(
        onRefresh: refreshProducts,
        child: GridView.count(
          crossAxisCount: 2,
          childAspectRatio: 240 / 440,
          crossAxisSpacing: 8,
          mainAxisSpacing: 8,
          children: List.generate(productsList.length, (index) {
            return ChangeNotifierProvider.value(
              value: productsList[index],
              child: FeedProducts(),
            );
          }),
        ),
      ),
    );
  }

  Future<void> refreshProducts() async {
    await Provider.of<Products>(context, listen: false).fetchProducts();
    setState(() {});
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
