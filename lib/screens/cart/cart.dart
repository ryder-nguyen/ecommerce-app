import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/constants/my_icons.dart';
import 'package:ecommerce_app/provider/cart_provider.dart';
import 'package:ecommerce_app/screens/cart/cart_empty.dart';
import 'package:ecommerce_app/screens/cart/cart_full.dart';
import 'package:ecommerce_app/screens/orders/order.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:ecommerce_app/services/payment.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class CartScreen extends StatefulWidget {
  static const routeName = '/cart';

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  final GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();
  final globalMethod = GlobalMethod();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  void initState() {
    super.initState();
    StripeService.init();
  }

  void payWithCard(int amount, CartProvider cartProvider) async {
    ProgressDialog dialog = ProgressDialog(context);
    dialog.style(
        message: 'Đang tiến hành thanh toán...',
        messageTextStyle: GoogleFonts.mcLaren());
    await dialog.show();
    var resultResponse = await StripeService.paymentWithNewCard(amount, 'USD');
    await dialog.hide();
    String resultMessage;
    if (resultResponse['status'] == "succeeded") {
      saveOrder(cartProvider);
      cartProvider.clearCart();
      resultMessage = "Thanh toán thành công";
    } else {
      resultMessage = "Thanh toán thất bại";
    }
    globalMethod.showPaymentResult(resultMessage, context);
  }

  void saveOrder(CartProvider cartProvider) {
    try{
      final orderId = Uuid().v4();
      cartProvider.getCartItems
          .forEach((key, orderValue) async {
        final id = Uuid().v4();
        await FirebaseFirestore.instance
            .collection('orders')
            .doc(id)
            .set({
          'orderId': orderId,
          'userId': _auth.currentUser.uid,
          'productId': orderValue.productId,
          'title': orderValue.title,
          'price': orderValue.price * orderValue.quantity,
          'imageUrl': orderValue.imageUrl,
          'quantity': orderValue.quantity,
          'orderDate': Timestamp.now(),
        });
      });

      Navigator.pushNamed(context, OrderScreen.routeName);

    }catch(err){
      print('upload order: $err');
    }
  }

  @override
  Widget build(BuildContext context) {
    final cartProvider = Provider.of<CartProvider>(context);
    return cartProvider.getCartItems.isEmpty
        ? Scaffold(
            appBar: AppBar(
              backgroundColor: Theme.of(context).cardColor,
              elevation: 0,
              centerTitle: true,
              title: Text(
                'Giỏ hàng'.toUpperCase(),
                style: GoogleFonts.mcLaren(
                    fontSize: 16, fontWeight: FontWeight.normal),
              ),
            ),
            key: globalKey,
            body: CartEmpty(),
          )
        : Scaffold(
            key: globalKey,
            appBar: AppBar(
              backgroundColor: Theme.of(context).backgroundColor,
              title: Text('Giỏ hàng (${cartProvider.getCartItems.length})', style: GoogleFonts.mcLaren(),),
              actions: [
                IconButton(
                  onPressed: () {
                    globalMethod.showDialogg(
                        'Xóa giở hàng!',
                        'Giỏ hàng của bạn sẽ bị xóa!',
                        () => cartProvider.clearCart(),
                        context);
                  },
                  icon: Icon(MyAppIcons.trash),
                )
              ],
            ),
            body: Container(
              margin: EdgeInsets.only(bottom: 60),
              child: ListView.builder(
                  itemCount: cartProvider.getCartItems.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ChangeNotifierProvider.value(
                      value: cartProvider.getCartItems.values.toList()[index],
                      child: CartFull(
                          productId:
                              cartProvider.getCartItems.keys.toList()[index]),
                    );
                  }),
            ),
            bottomSheet: checkoutSection(context, cartProvider),
          );
  }

  Widget checkoutSection(BuildContext context, CartProvider cartProvider) {
    final cartProvider = Provider.of<CartProvider>(context);
    var uuid = Uuid();
    return Container(
      decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Colors.grey, width: 0.5))),
      child: Padding(
        padding: EdgeInsets.all(8),
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    gradient: LinearGradient(colors: [
                      MyColors.gradiendLStart,
                      MyColors.gradiendLEnd
                    ], stops: [
                      0.0,
                      0.7
                    ])),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(30),
                    splashColor: Colors.red,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Thanh toán',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.mcLaren(
                            color: Theme.of(context).textSelectionColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    onTap: () async {
                      payWithCard(cartProvider.totalAmount.toInt() * 100, cartProvider);
                    },
                  ),
                ),
              ),
            ),
            Spacer(),
            Text(
              'Tổng cộng: ',
              textAlign: TextAlign.center,
              style: GoogleFonts.mcLaren(
                  color: Theme.of(context).textSelectionColor,
                  fontSize: 14,
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              '${cartProvider.totalAmount.toStringAsFixed(0)} ',
              //textAlign: TextAlign.center,
              style: GoogleFonts.mcLaren(
                  color: Colors.blue,
                  fontSize: 14,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }
}
