import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/inner_screens/product_details.dart';
import 'package:ecommerce_app/models/cart_arr.dart';
import 'package:ecommerce_app/provider/cart_provider.dart';
import 'package:ecommerce_app/provider/dark_theme_provider.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CartFull extends StatefulWidget {
  final String productId;

  const CartFull({this.productId});
  @override
  _CartFullState createState() => _CartFullState();
}

class _CartFullState extends State<CartFull> {
  GlobalMethod globalMethod = GlobalMethod();
  @override
  Widget build(BuildContext context) {
    final cartProvider = Provider.of<CartProvider>(context);
    final cartAtt = Provider.of<CartAttr>(context);
    final subTotal = cartAtt.price * cartAtt.quantity;
    return InkWell(
      onTap: () => Navigator.of(context)
          .pushNamed(ProductDetails.routeName, arguments: widget.productId),
      child: Container(
        height: 140,
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Theme.of(context).backgroundColor,
        ),
        child: Row(
          children: [
            Container(
              width: 130,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8)),
                image: DecorationImage(
                    image: NetworkImage(cartAtt.imageUrl), fit: BoxFit.cover
                    //fit: BoxFit.fill,
                    ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            cartAtt.title,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.mcLaren(
                                fontWeight: FontWeight.w600, fontSize: 15),
                          ),
                        ),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(32.0),
                            // splashColor: ,
                            onTap: () {
                              globalMethod.showDialogg(
                                  'Xóa mặt hàng!',
                                  'Mặt hàng này sẽ bị xóa khỏi giỏ hàng',
                                  () =>
                                      cartProvider.removeItem(widget.productId),
                                  context);
                            },
                            child: Container(
                              height: 50,
                              width: 50,
                              child: Icon(
                                Entypo.cross,
                                color: Colors.red,
                                size: 22,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          'Giá: ',
                          style: GoogleFonts.mcLaren(
                              color: Theme.of(context).textSelectionColor),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          '${cartAtt.price.toStringAsFixed(0)}',
                          style: GoogleFonts.mcLaren(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          'Tổng cộng: ',
                          style: GoogleFonts.mcLaren(
                              color: Theme.of(context).textSelectionColor),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          '${subTotal.toStringAsFixed(0)} ',
                          style: GoogleFonts.mcLaren(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Theme.of(context).textSelectionColor),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          'Số lượng: ',
                          style: GoogleFonts.mcLaren(
                              color: Theme.of(context).textSelectionColor),
                        ),
                        Spacer(),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(4.0),
                            // splashColor: ,
                            onTap: cartAtt.quantity < 2
                                ? null
                                : () {
                                    cartProvider.reduceItemByOne(
                                      widget.productId,
                                    );
                                  },
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Entypo.minus,
                                  color: cartAtt.quantity < 2
                                      ? Colors.grey
                                      : Colors.red,
                                  size: 22,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Card(
                          elevation: 12,
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.12,
                            padding: const EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                MyColors.gradiendLStart,
                                MyColors.gradiendLEnd,
                              ], stops: [
                                0.0,
                                0.7
                              ]),
                            ),
                            child: Text(
                              '${cartAtt.quantity}',
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(4.0),
                            // splashColor: ,
                            onTap: () {
                              cartProvider.addProductToCart(
                                  widget.productId,
                                  cartAtt.price,
                                  cartAtt.title,
                                  cartAtt.imageUrl);
                            },
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Entypo.plus,
                                  color: Colors.green,
                                  size: 22,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
