import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/inner_screens/product_details.dart';
import 'package:ecommerce_app/models/fav_attr.dart';
import 'package:ecommerce_app/models/product.dart';
import 'package:ecommerce_app/provider/fav_provider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class WishlistFull extends StatefulWidget {
  final String productId;

  WishlistFull({this.productId});

  @override
  _WishlistFullState createState() => _WishlistFullState();
}

class _WishlistFullState extends State<WishlistFull> {
  @override
  Widget build(BuildContext context) {
    final favProvider = Provider.of<FavProvider>(context);
    final favAttr = Provider.of<FavAttr>(context);

    return Stack(
      children: [
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(right: 30, bottom: 10),
          child: Material(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(5),
            elevation: 3,
            child: InkWell(
              onTap: () {
                Navigator.pushNamed(context, ProductDetails.routeName,
                    arguments: '${favAttr.productId}');
              },
              child: Container(
                padding: EdgeInsets.all(16),
                child: Row(
                  children: [
                    Container(
                      height: 80,
                      child: Image.network(
                          favAttr.imageUrl),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            favAttr.title,
                            style: GoogleFonts.mcLaren(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(' ${favAttr.price}',
                              style: GoogleFonts.mcLaren(
                                  fontWeight: FontWeight.bold, fontSize: 18))
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        positionedRemove(favProvider)
      ],
    );
  }

  Widget positionedRemove(FavProvider favProvider) {
    return Positioned(
      top: 20,
      right: 15,
      child: Container(
        height: 30,
        width: 30,
        child: MaterialButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          padding: EdgeInsets.all(0.0),
          color: MyColors.favColor,
          child: Icon(
            Icons.clear,
            color: Colors.white,
          ),
          onPressed: () {
            print('${widget.productId}');
            favProvider.removeItemFav(widget.productId);
          },
        ),
      ),
    );
  }
}
