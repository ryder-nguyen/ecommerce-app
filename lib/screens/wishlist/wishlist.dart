import 'package:ecommerce_app/provider/fav_provider.dart';
import 'package:ecommerce_app/screens/wishlist/wishlist_empty.dart';
import 'package:ecommerce_app/screens/wishlist/wishlist_full.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class WishListScreen extends StatelessWidget {
  static const routeName = '/wishlist';
  @override
  Widget build(BuildContext context) {
    final favProvider = Provider.of<FavProvider>(context);
    return favProvider.getFavItems.isEmpty
        ? Scaffold(
            appBar: AppBar(
              backgroundColor: Theme.of(context).cardColor,
              elevation: 0,
              centerTitle: true,
              title: Text(
                'Yêu thích',
                style: GoogleFonts.mcLaren(
                    fontSize: 16, fontWeight: FontWeight.normal),
              ),
            ),
            body: WishListEmpty(),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text(
                'Yêu thích (${favProvider.getFavItems.length})',
                style: GoogleFonts.mcLaren(),
              ),
            ),
            body: ListView.builder(
                itemCount: favProvider.getFavItems.length,
                itemBuilder: (BuildContext context, int index) {
                  return ChangeNotifierProvider.value(
                      value: favProvider.getFavItems.values.toList()[index],
                      child: WishlistFull(
                          productId:
                              favProvider.getFavItems.keys.toList()[index]));
                }));
  }
}
