import 'dart:math';

import 'package:backdrop/app_bar.dart';
import 'package:backdrop/button.dart';
import 'package:backdrop/scaffold.dart';
import 'package:backdrop/sub_header.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/constants/my_icons.dart';
import 'package:ecommerce_app/inner_screens/brands_navigation_rail.dart';
import 'package:ecommerce_app/provider/products.dart';
import 'package:ecommerce_app/screens/feeds.dart';
import 'package:ecommerce_app/screens/user.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:ecommerce_app/widget/backlayer.dart';
import 'package:ecommerce_app/widget/category.dart';
import 'package:ecommerce_app/widget/popular_products.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/home';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with AutomaticKeepAliveClientMixin<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    final productsData = Provider.of<Products>(context);
    return Scaffold(
      body: FutureBuilder (
        future: Future.wait([productsData.fetchProducts(), productsData.fetchBanners(), productsData.fetchBranches()]),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Center(
              child: BackdropScaffold(
                backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                headerHeight: MediaQuery.of(context).size.height * 0.25,
                appBar: BackdropAppBar(
                  title: Text("Trang chủ", style: GoogleFonts.mcLaren(
                      color: Colors.white
                  ),),
                  leading: BackdropToggleButton(
                    icon: AnimatedIcons.home_menu,
                  ),
                  flexibleSpace: Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [MyColors.starterColor, MyColors.endColor])),
                  ),
                  actions: <Widget>[
                    IconButton(
                      iconSize: 15,
                      padding: EdgeInsets.all(10),
                      icon: CircleAvatar(
                        radius: 15,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                            radius: 13,
                            backgroundImage: NetworkImage(
                              GlobalMethod.userModel == null ? '' : GlobalMethod.userModel.imageUrl,
                            )),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, UserScreen.routeName);
                      },
                    )
                  ],
                ),
                backLayer: BackLayerMenu(),
                frontLayer: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: double.infinity,
                        height: 190,
                        child: Carousel(
                          boxFit: BoxFit.fill,
                          autoplay: true,
                          animationCurve: Curves.fastOutSlowIn,
                          animationDuration: Duration(milliseconds: 1000),
                          dotSize: 6.0,
                          dotIncreasedColor: Colors.purple,
                          dotBgColor: Colors.black.withOpacity(0.2),
                          dotPosition: DotPosition.bottomCenter,
                          showIndicator: true,
                          indicatorBgPadding: 5.0,
                          images: productsData.banners.map(
                                  (imageUrl) => NetworkImage(imageUrl)
                          ).toList(),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Danh mục',
                          style: GoogleFonts.mcLaren(fontWeight: FontWeight.w800, fontSize: 20),
                        ),
                      ),
                      Container(
                        height: 180,
                        width: double.infinity,
                        child: ListView.builder(
                            itemCount: 4,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              return CategoryWidget(
                                index: index,
                              );
                            }),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Text(
                              'Nhãn hàng phổ biến',
                              style: GoogleFonts.mcLaren(
                                  fontWeight: FontWeight.w800, fontSize: 20),
                            ),
                            Spacer(),
                            FlatButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            BrandNavigationRailScreen(
                                              brandList: productsData.brands,
                                              index: 4,
                                            )));
                              },
                              child: Text(
                                'Tất cả...',
                                style: GoogleFonts.mcLaren(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 14,
                                    color: Colors.red),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 210,
                        width: MediaQuery.of(context).size.width * 0.95,
                        child: Swiper(
                          viewportFraction: 0.8,
                          scale: 0.9,
                          itemBuilder: (BuildContext context, int index) {
                            return ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage('assets/images/brand_background.jpg'),
                                        fit: BoxFit.cover
                                    )
                                ),
                                child: Center(
                                  child: Text(productsData.brands[index], style: GoogleFonts.mcLaren(
                                      fontSize: 32,
                                      color: Colors.white
                                  ),),
                                ),
                              ),
                            );
                          },
                          itemCount: productsData.brands.length,
                          autoplay: true,
                          onTap: (index) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BrandNavigationRailScreen(
                                      brandList: productsData.brands,
                                      index: index
                                    )));
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Text(
                              'Sản phẩm phổ biến',
                              style: GoogleFonts.mcLaren(
                                  fontWeight: FontWeight.w800, fontSize: 20),
                            ),
                            Spacer(),
                            FlatButton(
                              onPressed: () {
                                Navigator.of(context)
                                    .pushNamed(FeedScreen.routeName, arguments: 'popular');
                              },
                              child: Text(
                                'Tất cả...',
                                style: GoogleFonts.mcLaren(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 14,
                                    color: Colors.red),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 285,
                        margin: EdgeInsets.symmetric(horizontal: 3),
                        child: ListView.builder(
                          itemBuilder: (BuildContext context, int index) {
                            return ChangeNotifierProvider.value(
                              value: productsData.popularProducts[index],
                              child: PopularProducts(),
                            );
                          },
                          scrollDirection: Axis.horizontal,
                          itemCount: productsData.popularProducts.length,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
