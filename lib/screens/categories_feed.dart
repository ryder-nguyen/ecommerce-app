import 'package:ecommerce_app/models/product.dart';
import 'package:ecommerce_app/provider/products.dart';
import 'package:ecommerce_app/widget/feed_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CategoriesFeedScreen extends StatelessWidget {
  static const routeName = '/CategoriesFeedScreen';

  @override
  Widget build(BuildContext context) {
    final productsProvider = Provider.of<Products>(context, listen: false);
    final categoryName = ModalRoute.of(context).settings.arguments as String;
    print(categoryName);
    List<Product> productsList = productsProvider.findByCategory(categoryName);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).cardColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          categoryName.toUpperCase(),
          style:
              GoogleFonts.mcLaren(fontSize: 16, fontWeight: FontWeight.normal),
        ),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 240 / 440,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
        children: List.generate(productsList.length, (index) {
          return ChangeNotifierProvider.value(
            value: productsList[index],
            child: FeedProducts(),
          );
        }),
      ),
    );
  }
}
