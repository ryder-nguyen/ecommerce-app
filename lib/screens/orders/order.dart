import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/constants/my_icons.dart';
import 'package:ecommerce_app/provider/cart_provider.dart';
import 'package:ecommerce_app/provider/orders_provider.dart';
import 'package:ecommerce_app/screens/orders/order_empty.dart';
import 'package:ecommerce_app/screens/orders/order_full.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:ecommerce_app/services/payment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

class OrderScreen extends StatefulWidget {
  static const routeName = '/order';

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  final GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();
  final globalMethod = GlobalMethod();
  @override
  void initState() {
    super.initState();
    StripeService.init();
  }

  void payWithCard(int amount, CartProvider cartProvider) async {
    ProgressDialog dialog = ProgressDialog(context);
    dialog.style(
        message: 'Đang tiến hành thanh toán...',
        messageTextStyle: GoogleFonts.mcLaren());
    await dialog.show();
    var resultResponse = await StripeService.paymentWithNewCard(amount, 'USD');
    await dialog.hide();
    String resultMessage;
    if (resultResponse['status'] == "succeeded") {
      resultMessage = "Thanh toán thành công";
      cartProvider.clearCart();
    } else {
      resultMessage = "Thanh toán thất bại";
    }
    globalMethod.showPaymentResult(resultMessage, context);
  }

  @override
  Widget build(BuildContext context) {
    GlobalMethod globalMethod = GlobalMethod();
    final ordersProvider = Provider.of<OrdersProvider>(context);
    return FutureBuilder(
        future: ordersProvider.fetchOrders(),
        builder: (context, snapShot) {
          return ordersProvider.orders.isEmpty
              ? Scaffold(
                  appBar: AppBar(
                    backgroundColor: Theme.of(context).cardColor,
                    elevation: 0,
                    centerTitle: true,
                    title: Text(
                      'Lịch sử mua hàng'.toUpperCase(),
                      style: GoogleFonts.mcLaren(
                          fontSize: 16, fontWeight: FontWeight.normal),
                    ),
                  ),
                  key: globalKey,
                  body: OrderEmpty(),
                )
              : Scaffold(
                  key: globalKey,
                  appBar: AppBar(
                    backgroundColor: Theme.of(context).backgroundColor,
                    title: Text('Đã mua (${ordersProvider.orders.length})'),
                  ),
                  body: Container(
                    child: ListView.builder(
                        itemCount: ordersProvider.orders.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ChangeNotifierProvider.value(
                            value: ordersProvider.orders[index],
                            child: OrderFull(),
                          );
                        }),
                  ));
        });
  }
}
