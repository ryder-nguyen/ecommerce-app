import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/inner_screens/product_details.dart';
import 'package:ecommerce_app/models/cart_arr.dart';
import 'package:ecommerce_app/models/order_arr.dart';
import 'package:ecommerce_app/provider/cart_provider.dart';
import 'package:ecommerce_app/provider/dark_theme_provider.dart';
import 'package:ecommerce_app/provider/orders_provider.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class OrderFull extends StatefulWidget {
  @override
  _OrderFullState createState() => _OrderFullState();
}

class _OrderFullState extends State<OrderFull> {
  GlobalMethod globalMethod = GlobalMethod();
  @override
  Widget build(BuildContext context) {
    final ordersProvider = Provider.of<OrdersProvider>(context);
    final ordersAttrProvider = Provider.of<OrderAttr>(context);
    return InkWell(
      onTap: () => Navigator.of(context)
          .pushNamed(ProductDetails.routeName, arguments: ordersAttrProvider.productId),
      child: Container(
        height: 150,
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Theme.of(context).backgroundColor,
        ),
        child: Row(
          children: [
            Container(
              width: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)),
                image: DecorationImage(
                  image: NetworkImage(ordersAttrProvider.imageUrl),
                  fit: BoxFit.cover
                  //fit: BoxFit.fill,
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            ordersAttrProvider.title,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.mcLaren(
                                fontWeight: FontWeight.w600, fontSize: 15),
                          ),
                        ),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(32.0),
                            // splashColor: ,
                            onTap: () {
                              // globalMethod.showDialogg(
                              //     'Xóa mặt hàng!',
                              //     'Mặt hàng này sẽ bị xóa khỏi giỏ hàng',
                              //     () =>
                              //         ordersProvider.removeItem(widget.productId),
                              //     context);
                            },
                            child: Container(
                              height: 50,
                              width: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text('Giá: ', style: GoogleFonts.mcLaren(
                            color: Theme.of(context).textSelectionColor
                        ),),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          '${ordersAttrProvider.price}',
                          style: GoogleFonts.mcLaren(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),

                    Row(
                      children: [
                        Text('Số lượng: ', style: GoogleFonts.mcLaren(
                            color: Theme.of(context).textSelectionColor
                        ),),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'x${ordersAttrProvider.quantity}',
                          style: GoogleFonts.mcLaren(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),

                    Row(
                      children: [
                        Flexible(
                          child: Text('Order Id: ', style: GoogleFonts.mcLaren(
                              color: Theme.of(context).textSelectionColor
                          ),),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Text(
                            '${ordersAttrProvider.orderId}',
                            style: GoogleFonts.mcLaren(
                                fontSize: 10, fontWeight: FontWeight.w600, color: Colors.blue),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
