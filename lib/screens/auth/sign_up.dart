import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class SignUpScreen extends StatefulWidget {
  static const routeName = '/SignUpScreen';
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final FocusNode _passwordFocusNode = FocusNode();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _phoneNumberFocusNode = FocusNode();
  bool _obscureText = true;
  String _emailAddress = '';
  String _password = '';
  String _fullName = '';
  int _phoneNumber;
  File _pickedImage;
  String url;
  final _formKey = GlobalKey<FormState>();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final _globalMethod = GlobalMethod();
  bool isLoading = false;

  @override
  void dispose() {
    _passwordFocusNode.dispose();
    _emailFocusNode.dispose();
    _phoneNumberFocusNode.dispose();
    super.dispose();
  }

  void _submitForm() async {
    final isValid = _formKey.currentState.validate();
    FocusScope.of(context).unfocus();
    var date = DateTime.now().toString();
    var dateParse = DateTime.parse(date);
    var formattedDate =
        "${dateParse.year}-${dateParse.month}-${dateParse.year}";
    if (isValid) {
      _formKey.currentState.save();
      try {
        if(_pickedImage == null) {
          _globalMethod.showAuthError('Vui lòng chọn một ảnh!', context);
        } else {

          setState(() {
            isLoading = true;
          });

          final ref = FirebaseStorage.instance.ref().child('usersimages')
              .child(_fullName + '.jpg');
          await ref.putFile(_pickedImage);
          url = await ref.getDownloadURL();

          await _auth.createUserWithEmailAndPassword(
              email: _emailAddress.toLowerCase().trim(),
              password: _password.trim());
          final User user = _auth.currentUser;
          final _uid = user.uid;
          user.updateProfile(photoURL: url, displayName: _fullName);
          user.reload();

          await FirebaseFirestore.instance.collection('users')
              .doc(_uid).set({
            'id': _uid,
            'name': _fullName,
            'email': _emailAddress,
            'phoneNumber': _phoneNumber,
            'imageUrl': url,
            'joinedAt': formattedDate,
            'created': Timestamp.now()
          });

          // ignore: unnecessary_statements
          Navigator.canPop(context) ? Navigator.pop(context) : null;

        }
      } catch (error) {
        _globalMethod.showAuthError('${error.message}', context);
      } finally {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  void _pickImageCamera() async {
    final picker = ImagePicker();
    final pickedImage = await picker.getImage(source: ImageSource.camera, imageQuality: 10);
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });
    Navigator.pop(context);
  }

  void _pickImageGallery() async {
    final picker = ImagePicker();
    final pickedImage = await picker.getImage(source: ImageSource.gallery, imageQuality: 10);
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });
    Navigator.pop(context);
  }

  void _remove() {
    setState(() {
      _pickedImage = null;
    });
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.95,
              child: RotatedBox(
                quarterTurns: 2,
                child: WaveWidget(
                  config: CustomConfig(
                    gradients: [
                      [MyColors.gradiendFStart, MyColors.gradiendLStart],
                      [MyColors.gradiendFEnd, MyColors.gradiendLEnd],
                    ],
                    durations: [19440, 10800],
                    heightPercentages: [0.20, 0.25],
                    blur: MaskFilter.blur(BlurStyle.solid, 10),
                    gradientBegin: Alignment.bottomLeft,
                    gradientEnd: Alignment.topRight,
                  ),
                  waveAmplitude: 0,
                  size: Size(
                    double.infinity,
                    double.infinity,
                  ),
                ),
              ),
            ),
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 30, horizontal: 30),
                        child: CircleAvatar(
                          radius: 71,
                          backgroundColor: MyColors.gradiendLEnd,
                          child: CircleAvatar(
                            radius: 65,
                            backgroundImage: _pickedImage == null
                                ? null
                                : FileImage(_pickedImage),
                          ),
                        ),
                      ),
                      Positioned(
                          top: 120,
                          left: 120,
                          child: RawMaterialButton(
                            elevation: 10,
                            fillColor: MyColors.gradiendLEnd,
                            child: Icon(Icons.add_a_photo),
                            padding: EdgeInsets.all(15),
                            shape: CircleBorder(),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: Text(
                                        'Lựa chọn',
                                        style: GoogleFonts.mcLaren(
                                            fontWeight: FontWeight.w600,
                                            color: MyColors.gradiendLStart),
                                      ),
                                      content: SingleChildScrollView(
                                        child: ListBody(
                                          children: [
                                            InkWell(
                                              onTap: _pickImageCamera,
                                              splashColor: Colors.purpleAccent,
                                              child: Row(
                                                children: [
                                                  Padding(
                                                      padding:
                                                          EdgeInsets.all(8),
                                                      child: Icon(
                                                        Icons.camera,
                                                        color:
                                                            Colors.purpleAccent,
                                                      )),
                                                  Text(
                                                    'Camera',
                                                    style: GoogleFonts.mcLaren(
                                                        fontSize: 18,
                                                        color: MyColors.title,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  )
                                                ],
                                              ),
                                            ),
                                            InkWell(
                                              onTap: _pickImageGallery,
                                              splashColor: Colors.purpleAccent,
                                              child: Row(
                                                children: [
                                                  Padding(
                                                      padding:
                                                          EdgeInsets.all(8),
                                                      child: Icon(
                                                        Icons.image,
                                                        color:
                                                            Colors.purpleAccent,
                                                      )),
                                                  Text(
                                                    'Thư viện ảnh',
                                                    style: GoogleFonts.mcLaren(
                                                        fontSize: 18,
                                                        color: MyColors.title,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  )
                                                ],
                                              ),
                                            ),
                                            InkWell(
                                              onTap: _remove,
                                              splashColor: Colors.red,
                                              child: Row(
                                                children: [
                                                  Padding(
                                                      padding:
                                                          EdgeInsets.all(8),
                                                      child: Icon(
                                                        Icons.remove_circle,
                                                        color: Colors.red,
                                                      )),
                                                  Text(
                                                    'Xóa ảnh',
                                                    style: GoogleFonts.mcLaren(
                                                        fontSize: 18,
                                                        color: MyColors.title,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  });
                            },
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: TextFormField(
                              style: GoogleFonts.mcLaren(),
                              key: ValueKey('name'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Tên không thể để trống';
                                }
                                return null;
                              },
                              textInputAction: TextInputAction.next,
                              onEditingComplete: () => FocusScope.of(context)
                                  .requestFocus(_emailFocusNode),
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  border: const UnderlineInputBorder(),
                                  filled: true,
                                  prefixIcon: Icon(Icons.person),
                                  labelText: 'Tên đầy đủ',
                                  labelStyle: GoogleFonts.mcLaren(),
                                  fillColor: Theme.of(context).backgroundColor),
                              onSaved: (value) {
                                _fullName = value;
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: TextFormField(
                                style: GoogleFonts.mcLaren(),
                              key: ValueKey('email'),
                              focusNode: _emailFocusNode,
                              validator: (value) {
                                if (value.isEmpty || !value.contains('@')) {
                                  return 'Vui lòng nhập đúng địa chỉ email';
                                }
                                return null;
                              },
                              textInputAction: TextInputAction.next,
                              onEditingComplete: () => FocusScope.of(context)
                                  .requestFocus(_passwordFocusNode),
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  labelStyle: GoogleFonts.mcLaren(),
                                  border: const UnderlineInputBorder(),
                                  filled: true,
                                  prefixIcon: Icon(Icons.email),
                                  labelText: 'Địa chỉ email',
                                  fillColor: Theme.of(context).backgroundColor),
                              onSaved: (value) {
                                _emailAddress = value;
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: TextFormField(
                              style: GoogleFonts.mcLaren(),
                              key: ValueKey('Password'),
                              validator: (value) {
                                if (value.isEmpty || value.length < 7) {
                                  return 'Vui lòng nhập đúng mật khẩu';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.emailAddress,
                              focusNode: _passwordFocusNode,
                              decoration: InputDecoration(
                                  labelStyle: GoogleFonts.mcLaren(),
                                  border: const UnderlineInputBorder(),
                                  filled: true,
                                  prefixIcon: Icon(Icons.lock),
                                  suffixIcon: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    child: Icon(_obscureText
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                  ),
                                  labelText: 'Mật khẩu',
                                  fillColor: Theme.of(context).backgroundColor),
                              onSaved: (value) {
                                _password = value;
                              },
                              obscureText: _obscureText,
                              onEditingComplete: () => FocusScope.of(context)
                                  .requestFocus(_phoneNumberFocusNode),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: TextFormField(
                              style: GoogleFonts.mcLaren(),
                              key: ValueKey('phone number'),
                              focusNode: _phoneNumberFocusNode,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Vui lòng nhập đúng số điện thoại';
                                }
                                return null;
                              },
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              textInputAction: TextInputAction.next,
                              onEditingComplete: _submitForm,
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                  labelStyle: GoogleFonts.mcLaren(),
                                  border: const UnderlineInputBorder(),
                                  filled: true,
                                  prefixIcon: Icon(Icons.phone_android),
                                  labelText: 'Số điện thoại',
                                  fillColor: Theme.of(context).backgroundColor),
                              onSaved: (value) {
                                _phoneNumber = int.parse(value);
                              },
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              isLoading
                                  ? Container(
                                      margin:
                                          EdgeInsets.only(top: 15, right: 10),
                                      width: 20,
                                      height: 20,
                                      child: CircularProgressIndicator())
                                  : ElevatedButton(
                                      style: ButtonStyle(
                                          shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          side: BorderSide(
                                              color: MyColors.backgroundColor),
                                        ),
                                      )),
                                      onPressed: _submitForm,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Đăng ký',
                                            style: GoogleFonts.mcLaren(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 17),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Icon(
                                            Feather.user,
                                            size: 18,
                                          )
                                        ],
                                      )),
                              SizedBox(width: 20),
                            ],
                          ),
                        ],
                      ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
