import 'package:badges/badges.dart';
import 'package:ecommerce_app/constants/my_colors.dart';
import 'package:ecommerce_app/constants/my_icons.dart';
import 'package:ecommerce_app/provider/cart_provider.dart';
import 'package:ecommerce_app/provider/dark_theme_provider.dart';
import 'package:ecommerce_app/provider/fav_provider.dart';
import 'package:ecommerce_app/provider/products.dart';
import 'package:ecommerce_app/screens/cart/cart.dart';
import 'package:ecommerce_app/screens/wishlist/wishlist.dart';
import 'package:ecommerce_app/services/global_method.dart';
import 'package:ecommerce_app/services/payment.dart';
import 'package:ecommerce_app/widget/feed_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

class ProductDetails extends StatefulWidget {
  static const routeName = "/ProductDetails";
  const ProductDetails({Key key}) : super(key: key);

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final _globalMethod = GlobalMethod();
  @override
  Widget build(BuildContext context) {
    final themeState = Provider.of<DarkThemeProvider>(context);
    final productsData = Provider.of<Products>(context, listen: false);
    final productsList = productsData.products;
    final cartProvider = Provider.of<CartProvider>(context);
    final favProvider = Provider.of<FavProvider>(context);

    final productId = ModalRoute.of(context).settings.arguments as String;
    final productAttributes = productsData.findById(productId);

    print('productID: $productId');

    return Scaffold(
      body: Stack(
        children: [
          Container(
            foregroundDecoration: BoxDecoration(color: Colors.black12),
            height: MediaQuery.of(context).size.height * 0.45,
            width: double.infinity,
            child: Image.network(productAttributes.imageUrl, fit: BoxFit.cover,),
          ),
          SingleChildScrollView(
            padding: const EdgeInsets.only(top: 16, bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 250,
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Colors.purple.shade200,
                          onTap: () {},
                          borderRadius: BorderRadius.circular(30),
                          child: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Icon(
                              Icons.save,
                              size: 23,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Colors.purple.shade200,
                          onTap: () {},
                          borderRadius: BorderRadius.circular(30),
                          child: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Icon(
                              Icons.share,
                              size: 23,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Text(
                                productAttributes.title,
                                maxLines: 2,
                                style: GoogleFonts.mcLaren(
                                    fontSize: 28, fontWeight: FontWeight.w600),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              'Giá: ${productAttributes.price.toStringAsFixed(0)}',
                              style: GoogleFonts.mcLaren(
                                  color: themeState.darkMode
                                      ? Theme.of(context).disabledColor
                                      : MyColors.subTitle,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 21),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Divider(
                          thickness: 1,
                          color: Colors.grey,
                          height: 1,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16),
                        child: Text(
                          '${productAttributes.description}',
                          style: GoogleFonts.mcLaren(
                              fontWeight: FontWeight.w400,
                              fontSize: 21,
                              color: themeState.darkMode
                                  ? Theme.of(context).disabledColor
                                  : MyColors.subTitle),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Divider(
                          thickness: 1,
                          color: Colors.grey,
                          height: 1,
                        ),
                      ),
                      _details(themeState.darkMode, 'Hiệu: ',
                          '${productAttributes.brand}'),
                      _details(themeState.darkMode, 'Số lượng: ',
                          '${productAttributes.quantity}'),
                      _details(themeState.darkMode, 'Danh mục: ',
                          '${productAttributes.productCategoryName}'),
                      _details(themeState.darkMode, 'Phổ biến: ',
                          '${productAttributes.isPopular}'),
                      SizedBox(
                        height: 15,
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                        height: 1,
                      ),
                      Container(
                        color: Theme.of(context).backgroundColor,
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: Text(
                                'Chưa có review sản phẩm',
                                style: GoogleFonts.mcLaren(
                                    color: Theme.of(context).textSelectionColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 21),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: Text(
                                'Hãy thêm bình luận đầu tiên',
                                style: GoogleFonts.mcLaren(
                                    color: themeState.darkMode
                                        ? Theme.of(context).disabledColor
                                        : MyColors.subTitle,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20),
                              ),
                            ),
                            SizedBox(
                              height: 70,
                            ),
                            Divider(
                              thickness: 1,
                              color: Colors.grey,
                              height: 1,
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(8),
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Text(
                    'Đề xuất',
                    style: GoogleFonts.mcLaren(fontSize: 20, fontWeight: FontWeight.w700),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 30),
                  width: double.infinity,
                  color: Theme.of(context).scaffoldBackgroundColor,
                  height: 400,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: productsList.length < 7 ? productsList.length : 7,
                      itemBuilder: (BuildContext context, int index) {
                        return ChangeNotifierProvider.value(
                          value: productsList[index],
                          child: FeedProducts(),
                        );
                      }),
                )
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              centerTitle: true,
              title: Text(
                'Chi tiết'.toUpperCase(),
                style: GoogleFonts.mcLaren(fontSize: 16, fontWeight: FontWeight.normal),
              ),
              actions: [
                Consumer<FavProvider>(
                  builder: (_, favsProvider, ch) =>  Badge(
                    badgeColor: MyColors.cartBadgeColor,
                    animationType: BadgeAnimationType.slide,
                    toAnimate: true,
                    position: BadgePosition.topEnd(top: 5, end: 7),
                    badgeContent: Text(
                      favsProvider.getFavItems.length.toString(),
                      style: GoogleFonts.mcLaren(color: Colors.white),
                    ),
                    child: IconButton(
                      icon: Icon(
                        MyAppIcons.wishlist,
                        color: MyColors.favColor,
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                          WishListScreen.routeName,
                        );
                      },
                    ),
                  ),
                ),
               Consumer<CartProvider>(
                   builder: (_, cartsProvider, ch) =>  Badge(
                     badgeColor: MyColors.cartBadgeColor,
                     animationType: BadgeAnimationType.slide,
                     toAnimate: true,
                     position: BadgePosition.topEnd(top: 5, end: 7),
                     badgeContent: Text(
                       cartsProvider.getCartItems.length.toString(),
                       style: GoogleFonts.mcLaren(color: Colors.white),
                     ),
                     child: IconButton(
                       icon: Icon(
                         MyAppIcons.cart,
                         color: Colors.red,
                       ),
                       onPressed: () {
                         Navigator.of(context).pushNamed(CartScreen.routeName);
                       },
                     ),
                   )
               )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 60,
                    child: RaisedButton(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      shape: RoundedRectangleBorder(side: BorderSide.none),
                      color: Colors.redAccent.shade400,
                      onPressed: cartProvider.getCartItems.containsKey(productId)
                          ? () {}
                          : () {
                              cartProvider.addProductToCart(
                                  productId,
                                  productAttributes.price,
                                  productAttributes.title,
                                  productAttributes.imageUrl);
                            },
                      child: Text(
                        cartProvider.getCartItems.containsKey(productId)
                            ? 'Đã thêm'
                            : 'Thêm vào giỏ'.toUpperCase(),
                        style: GoogleFonts.mcLaren(fontSize: 16, color: Colors.white),
                      ),
                    ),
                  ),
                ),
                // Expanded(
                //   flex: 3,
                //   child: Container(
                //     height: 50,
                //     child: RaisedButton(
                //       materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                //       shape: RoundedRectangleBorder(side: BorderSide.none),
                //       color: Theme.of(context).backgroundColor,
                //       onPressed: () {
                //         payWithCard(productAttributes.price.toInt() * 100);
                //       },
                //       child: Row(
                //         children: [
                //           Text(
                //             'Mua ngay'.toUpperCase(),
                //             style: GoogleFonts.mcLaren(fontSize: 16, color: Colors.black),
                //           ),
                //           SizedBox(
                //             width: 5,
                //           ),
                //           Icon(
                //             Icons.payment,
                //             color: Colors.green.shade700,
                //             size: 19,
                //           )
                //         ],
                //       ),
                //     ),
                //   ),
                // ),
                Expanded(
                  flex: 1,
                  child: Container(
                    color: themeState.darkMode
                        ? Theme.of(context).disabledColor
                        : MyColors.subTitle,
                    height: 60,
                    child: InkWell(
                      splashColor: MyColors.favColor,
                      onTap: () {
                        favProvider.addFav(productAttributes);
                      },
                      child: Center(
                        child: Icon(
                          favProvider.getFavItems.containsKey(productId)
                              ? Icons.favorite
                              : MyAppIcons.wishlist,
                          color: favProvider.getFavItems.containsKey(productId)
                              ? Colors.red
                              : Colors.white,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  void payWithCard(int amount) async {
    ProgressDialog dialog = ProgressDialog(context);
    dialog.style(
        message: 'Đang tiến hành thanh toán...',
        messageTextStyle: GoogleFonts.mcLaren());
    await dialog.show();
    var resultResponse = await StripeService.paymentWithNewCard(amount, 'USD');
    await dialog.hide();
    _globalMethod.showPaymentResult(resultResponse['status'], context);
  }

  Widget _details(bool isDarkMode, String title, String content) {
    return Padding(
      padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            title,
            style: GoogleFonts.mcLaren(
                color: Theme.of(context).textSelectionColor,
                fontWeight: FontWeight.w600,
                fontSize: 21),
          ),
          Text(
            content,
            style: GoogleFonts.mcLaren(
                color: isDarkMode
                    ? Theme.of(context).disabledColor
                    : MyColors.subTitle,
                fontWeight: FontWeight.w400,
                fontSize: 20),
          )
        ],
      ),
    );
  }
}
