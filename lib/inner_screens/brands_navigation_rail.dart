import 'package:ecommerce_app/provider/products.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'brands_rail_widget.dart';

class BrandNavigationRailScreen extends StatefulWidget {
  final List<String> brandList;
  final int index;

  BrandNavigationRailScreen({Key key, this.brandList, this.index}) : super(key: key);

  static const routeName = '/brands_navigation_rail';
  @override
  _BrandNavigationRailScreenState createState() =>
      _BrandNavigationRailScreenState();
}

class _BrandNavigationRailScreenState extends State<BrandNavigationRailScreen> {
  final _auth = FirebaseAuth.instance;
  int _selectedIndex = 0;
  final padding = 8.0;
  String routeArgs;
  String brand;

  @override
  void didChangeDependencies() {
    routeArgs = ModalRoute.of(context).settings.arguments.toString();
    _selectedIndex = widget.index;
    print(routeArgs.toString());
    brand = widget.brandList[_selectedIndex];
    super.didChangeDependencies();
  }

  @override
  void initState() {
    if(!widget.brandList.contains('Tất cả')) {
      widget.brandList.add('Tất cả');
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Row(
          children: <Widget>[
            LayoutBuilder(
              builder: (context, constraint) {
                return SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints:
                        BoxConstraints(minHeight: constraint.maxHeight),
                    child: IntrinsicHeight(
                      child: NavigationRail(
                        minWidth: 56.0,
                        groupAlignment: 1.0,
                        selectedIndex: _selectedIndex,
                        onDestinationSelected: (int index) {
                          setState(() {
                            _selectedIndex = index;
                            brand = widget.brandList[index];
                          });
                        },
                        labelType: NavigationRailLabelType.all,
                        leading: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: CircleAvatar(
                                radius: 16,
                                backgroundImage:
                                    NetworkImage(_auth.currentUser.photoURL),
                              ),
                            ),
                            SizedBox(
                              height: 80,
                            ),
                          ],
                        ),
                        selectedLabelTextStyle: GoogleFonts.mcLaren(
                          color: Color(0xffffe6bc97),
                          fontSize: 20,
                          letterSpacing: 1,
                          decoration: TextDecoration.underline,
                          decorationThickness: 2.5,
                        ),
                        unselectedLabelTextStyle: GoogleFonts.mcLaren(
                          fontSize: 15,
                          letterSpacing: 0.8,
                        ),
                        destinations: widget.brandList
                            .map((brandName) => buildRotatedTextRailDestination(
                                brandName, padding))
                            .toList(),
                      ),
                    ),
                  ),
                );
              },
            ),
            // This is the main content.

            ContentSpace(context, brand)
          ],
        ),
      ),
    );
  }
}

NavigationRailDestination buildRotatedTextRailDestination(
    String text, double padding) {
  return NavigationRailDestination(
    icon: SizedBox.shrink(),
    label: Padding(
      padding: EdgeInsets.symmetric(vertical: padding),
      child: RotatedBox(
        quarterTurns: -1,
        child: Text(text),
      ),
    ),
  );
}

class ContentSpace extends StatelessWidget {
  // final int _selectedIndex;

  final String brand;
  ContentSpace(BuildContext context, this.brand);

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    final productBrand = brand == 'Tất cả'
        ? productsData.products
        : productsData.findByBrand(brand);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 8, 0, 0),
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: ListView.builder(
            itemCount: productBrand.length,
            itemBuilder: (BuildContext context, int index) =>
                ChangeNotifierProvider.value(
                    value: productBrand[index], child: BrandsNavigationRail()),
          ),
        ),
      ),
    );
  }
}
