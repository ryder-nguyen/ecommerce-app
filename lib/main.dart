import 'package:ecommerce_app/bottom_bar.dart';
import 'package:ecommerce_app/constants/theme_data.dart';
import 'package:ecommerce_app/inner_screens/brands_navigation_rail.dart';
import 'package:ecommerce_app/inner_screens/product_details.dart';
import 'package:ecommerce_app/provider/cart_provider.dart';
import 'package:ecommerce_app/provider/dark_theme_provider.dart';
import 'package:ecommerce_app/provider/fav_provider.dart';
import 'package:ecommerce_app/provider/orders_provider.dart';
import 'package:ecommerce_app/provider/products.dart';
import 'package:ecommerce_app/screens/auth/login.dart';
import 'package:ecommerce_app/screens/auth/sign_up.dart';
import 'package:ecommerce_app/screens/cart/cart.dart';
import 'package:ecommerce_app/screens/categories_feed.dart';
import 'package:ecommerce_app/screens/feeds.dart';
import 'package:ecommerce_app/screens/landing_page.dart';
import 'package:ecommerce_app/screens/orders/order.dart';
import 'package:ecommerce_app/screens/upload_product_form.dart';
import 'package:ecommerce_app/screens/user.dart';
import 'package:ecommerce_app/screens/user_state.dart';
import 'package:ecommerce_app/screens/wishlist/wishlist.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  DarkThemeProvider themeChangeProvider = DarkThemeProvider();

  void getCurrentTheme() async {
    themeChangeProvider.darkMode =
        await themeChangeProvider.prefs.getTheme() ?? false;
  }

  @override
  void initState() {
    getCurrentTheme();
    super.initState();
  }

  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MaterialApp(
            home: Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return MaterialApp(
            home: Scaffold(
              body: Center(child: Text('${snapshot.error.toString()}')),
            ),
          );
        }

        return MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) {
              return themeChangeProvider;
            }),
            ChangeNotifierProvider(create: (_) => Products()),
            ChangeNotifierProvider(create: (_) => CartProvider()),
            ChangeNotifierProvider(create: (_) => FavProvider()),
            ChangeNotifierProvider(create: (_) => OrdersProvider()),
          ],
          child:
              Consumer<DarkThemeProvider>(builder: (context, themeData, child) {

            return MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: Styles.themeData(themeChangeProvider.darkMode, context),
              home: UserState(),
              routes: {
                BrandNavigationRailScreen.routeName: (context) => BrandNavigationRailScreen(),
                CartScreen.routeName: (context) => CartScreen(),
                FeedScreen.routeName: (context) => FeedScreen(),
                WishListScreen.routeName: (context) => WishListScreen(),
                ProductDetails.routeName: (context) => ProductDetails(),
                CategoriesFeedScreen.routeName: (context) => CategoriesFeedScreen(),
                LoginScreen.routeName: (context) => LoginScreen(),
                SignUpScreen.routeName: (context) => SignUpScreen(),
                BottomBarScreen.routeName: (context) => BottomBarScreen(),
                UploadProductForm.routeName: (context) => UploadProductForm(),
                UserScreen.routeName: (context) => UserScreen(),
                OrderScreen.routeName: (context) => OrderScreen(),
              },
            );
          }),
        );
      },
    );
  }
}
