import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce_app/models/user_model.dart';
import 'package:ecommerce_app/screens/user_state.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class GlobalMethod {

  static UserModel userModel;

  Future<void> saveData() async {
    User user = FirebaseAuth.instance.currentUser;

    final DocumentSnapshot userDoc = user.isAnonymous ? null : await FirebaseFirestore.instance.collection('users').doc(user.uid).get();
    if(userDoc == null) {
      return;
    } else {
      userModel = UserModel(
        name:  userDoc.get('name'),
        email: user.email,
        id: user.uid,
        address: userDoc.get('address'),
        imageUrl: user.photoURL,
        phoneNumber: userDoc.get('phoneNumber'),
        joinedAt: userDoc.get('joinedAt'),
      );
      print('user: ${userModel.address}');
    }
  }

  void resetData() {
    if(userModel != null) {
      userModel = null;
    }
  }

  Future<void> showDialogg(
      String title, String subTitle, Function fct, BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Row(
              children: [
                Padding(
                    padding: const EdgeInsets.only(right: 6),
                    child: Icon(
                      Icons.error_outline,
                      size: 20,
                      color: Colors.red,
                    )
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Text(title),
                )
              ],
            ),
            content: Text(subTitle),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('Hủy', style: GoogleFonts.mcLaren())),
              TextButton(
                  onPressed: () {
                    fct();
                    Navigator.pop(context);
                  },
                  child: Text('Ok', style: GoogleFonts.mcLaren())),
            ],
          );
        });
  }

  Future<void> showAuthError(String subTitle, BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 6),
                  child: Icon(
                    Icons.error_outline,
                    size: 20,
                    color: Colors.red,
                  )
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Text('Có lỗi xảy ra!', style: GoogleFonts.mcLaren()),
                )
              ],
            ),
            content: Text(subTitle),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Ok', style: GoogleFonts.mcLaren())),
            ],
          );
        });
  }

  Future<void> showPaymentResult(String subTitle, BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 6),
                  child: Icon(
                    Feather.shopping_cart,
                    size: 20,
                    color: Colors.red,
                  )
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Text('Thông tin thanh toán!', style: GoogleFonts.mcLaren()),
                )
              ],
            ),
            content: Text(subTitle),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Tiếp tục mua sắm', style: GoogleFonts.mcLaren())),
            ],
          );
        });
  }

  Future<void> confirmSignOut(BuildContext context, Function function) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 6),
                    child: Icon(
                      Icons.warning_amber_outlined,
                      size: 20,
                      color: Colors.red,
                    )
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Text('Lưu ý',  style: GoogleFonts.mcLaren()),
                )
              ],
            ),
            content: Text('Bạn muốn đăng xuất?',  style: GoogleFonts.mcLaren()),
            actions: [
              TextButton(onPressed: () {
                Navigator.of(context).canPop() ? Navigator.pop(context) : null;
              }, child: Text('Hủy', style: GoogleFonts.mcLaren())),
              TextButton(
                  onPressed: () async {
                    function.call();
                    Navigator.pop(context);
                  },
                  child: Text('Ok', style: GoogleFonts.mcLaren())),
            ],
          );
        });
  }
}
