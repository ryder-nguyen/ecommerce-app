
import 'package:cloud_functions/cloud_functions.dart';
import 'package:stripe_payment/stripe_payment.dart';

class StripeTransactionResponse {
  String message;
  bool success;
  StripeTransactionResponse({this.message, this.success});
}

class StripeService {
  static final HttpsCallable CREATE_PAYMENT_INTENT =
      FirebaseFunctions.instance.httpsCallable('createPaymentIntent');
  static final HttpsCallable CONFIRM_PAYMENT_INTENT =
      FirebaseFunctions.instance.httpsCallable('confirmPaymentIntent');
  static final HttpsCallable CREATE_CUSTOMER_CHARGE =
      FirebaseFunctions.instance.httpsCallable('createCustomerCharge');
  static final HttpsCallable GET_PAYMENT_INTENT =
      FirebaseFunctions.instance.httpsCallable('getUserPaymentIntent');
  static final publicKey =
      "pk_test_51JEXvuLGaoWMoA0dxI5F0rRMKMRJCnN4miJ7gWECaQgIEoSvsHiiJf8BXsc5RQ7in7WZdtFrekv3M4kdrV08MKG9005aEmLYPP";

  static init() {
    StripePayment.setOptions(StripeOptions(
        publishableKey: publicKey, merchantId: 'test', androidPayMode: 'test'));
  }

  static Future<Map<dynamic, dynamic>> paymentWithNewCard(
      int amount, String currency) async {
    var paymentMethod = await StripePayment.paymentRequestWithCardForm(
        CardFormPaymentRequest());

    var paymentIntent = await CREATE_PAYMENT_INTENT.call(<String, dynamic>{
      'methodId': paymentMethod.id,
      'amount': amount,
      'currency': currency
    }).then((response) {
      return response;
    }).catchError((error) {
      print("CREATE_PAYMENT_INTENT error: " + error.toString());
    });

    var resultResponse = await CONFIRM_PAYMENT_INTENT.call(<String, dynamic>{
      'paymentIntentId': paymentIntent.data['id']
    }).then((response) {
      print("CONFIRM_PAYMENT_INTENT result: " + response.data['status']);
      return response;
    }).catchError((error) {
      print("CONFIRM_PAYMENT_INTENT error: " + error.toString());
    });

    return resultResponse.data;
  }
}
